<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class BlogApiController extends AbstractController
{

    /**
     *  1.2 Matching HTTP Methods
     *
     * 2.1 Parameters Validation + {parameter_name<requirements>}
     */
    #[Route('/api/posts/{id<\d+>}',
        name: 'api_post_show',
        requirements: ['page' => '\d+'],
        methods: ['GET', 'HEAD']
    ),
    ]
    public function show(int $id): JsonResponse
    {
        // Логіка для відображення даних поста з ідентифікатором $id
        $postData = [
            'id' => $id,
            'title' => 'Example Post',
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ];

        return $this->json($postData);
    }


    #[Route('/api/posts/{id}', name: 'api_post_edit', methods: ['PUT'])]
    public function edit(Request $request, int $id): Response
    {
        // Отримати допис за його ідентифікатором $id і виконати редагування

        // Обробка даних з форми, надісланими методом PUT
        $data = json_decode($request->getContent(), true);

        // Перевірка наявності необхідних даних у запиті
        if (!isset($data['title']) || !isset($data['content'])) {
            return new Response('Missing title or content', Response::HTTP_BAD_REQUEST);
        }

        // Отримання даних з запиту
        $title = $data['title'];
        $content = $data['content'];

        // Здійснення логіки збереження змін до бази даних або інші дії
        $data = json_encode([
            'id' => $id,
            'title' => $title,
            'content' => $content
        ]);
        // Повернення відповіді з кодом успішності
        return new Response("Post updated successfully, data: $data", Response::HTTP_OK);
    }

    /**
     * 2.5 Special Parameters
     *
     * 2.6 Extra Parameters
     */

    #[Route(
        '/api/posts/{_locale}/search.{!_format}',
        name: 'api_post_search',
        requirements: ['_locale' => 'en|fr', '_format' => 'html|json'],
        defaults: ['title' => 'api search title'],
        locale: 'en',
        format: 'json'
    ),
    ]
    public function search(string $_locale, string $title): JsonResponse
    {
        // Логіка для відображення даних поста з ідентифікатором $id
        $postData = [
            '_locale' => $_locale,
            'titleDefault' => $title,
            'title' => 'Example _format',
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ];

        return $this->json($postData);
    }

    /**
     * 2.7 Slash Characters in Route Parameters
     */
    #[Route('/api/token/{token}', name: 'api_token', requirements: ['token' => '.+'])]
    public function token(string $token): JsonResponse
    {
        // Логіка для відображення даних поста з ідентифікатором $id
        $postData = [
            'token' => $token,
            'title' => 'Example Post',
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        ];

        return $this->json($postData);
    }

}
