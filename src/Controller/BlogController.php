<?php

namespace App\Controller;

use App\Enums\BlogStatusEnum;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\UriSigner;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * 2 Route Groups and Prefixes
 */
#[Route('/', name: 'blog_')]
class BlogController extends AbstractController
{
    private RouterInterface $router;
    private UriSigner $uriSigner;

    public function __construct(
        RouterInterface $router,
        UriSigner $uriSigner,
    ) // Change to RouterInterface
    {
        $this->router = $router;
        $this->uriSigner = $uriSigner;
    }
    /**
     *  1.1 Creating Routes
     */
    #[Route('/blog', name: 'index')]
    public function index(Request $request): Response
    {

        /**
         * 4. Getting the Route Name and Parameters
         */
        $routerName = $request->attributes->get('_route');
        $routeParameters = $request->attributes->get('_route_params');

        $data = json_encode([$routerName, $routeParameters]);

        /**
         * 6. Generating URLs in Controllers
         */
        // generate a URL with no route arguments
        $signUpPage = $this->generateUrl('blog_list');
        // generate a URL with route arguments
        $userProfilePage = $this->generateUrl('blog_show', [
            'slug' => rand(20,100),
            'testAttribute' => 'test attribute'
        ]);
        // argument to generate different URLs (e.g. an "absolute URL")
        $signUpPageAbs = $this->generateUrl('blog_index', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $signUpPageInDutch = $this->generateUrl('blog_index', ['_locale' => 'nl']);

        $routeName = 'blog_index';
        $routeParameters = ['_locale' => 'nl'];
        $url = null;
        try {
            $url = $this->router->generate($routeName, $routeParameters);
        } catch (RouteNotFoundException $e) {
            return new Response("the route is not defined...",404);
        }

        /**
         * 7. Signing URIs
         */
        $signedUrl = $this->uriSigner->sign($userProfilePage);

        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'type' => 'index',
            'request' => $data,
            'signUpPage' => $signUpPage,
            'userProfilePage' => $userProfilePage,
            'signUpPageAbs' => $signUpPageAbs,
            'signUpPageInDutch' => $signUpPageInDutch,
            'signedUrl' => $signedUrl,
            'url' => $url
        ]);
    }

    /**
     * 2.1 Route Parameters
     */
    #[Route('/blog/{slug}', name: 'show')]
    public function show(string $slug, Request $request): Response
    {
        $currentUrl = $request->getRequestUri();
        if ($this->uriSigner->check($currentUrl)) {
            // Підпис дійсний
            // Продовжуємо обробку запиту
            $request->getBasePath();
        } else {
            // Підпис недійсний
            // Вживаємо заходів, наприклад, повертаємо помилку
            return new Response('Недійсний підпис запиту.', 403);
        }

        return $this->render('blog/show.html.twig', [
            'slug' => $slug,
        ]);
    }

    /**
     * 2.4 Backed Enum Parameters
     */
    #[Route('/blog/posts-about-{category}/page/{pageNumber}', name: 'post_show')]
    public function postShow(string $category, BlogStatusEnum $pageNumber = BlogStatusEnum::Completed): Response
    {
        return $this->render('blog/post.html.twig', [
            'category' => $category,
            'pageNumber' => $pageNumber->name,
        ]);
    }

    /**
     * 2.3 Priority Parameter
     */
    #[Route('/blog/list', name: 'list', priority: 2)]
    public function list(Request $request): Response
    {
        $currentUrl = $request->getRequestUri();
        if ($this->uriSigner->check($currentUrl)) {
            // Підпис дійсний
            // Продовжуємо обробку запиту
            $request->getBasePath();
        } else {
            // Підпис недійсний
            // Вживаємо заходів, наприклад, повертаємо помилку
            return new Response('Недійсний підпис запиту.', 403);
        }

        return $this->render('blog/list.html.twig', [
            'controller_name' => 'BlogController',
            'type' => 'list'
        ]);
    }
}
