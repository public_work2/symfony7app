<?php

namespace App\Enums;

/**
 * 2.4 Backed Enum Parameters
 */
enum BlogStatusEnum: string {
    case Newsletter = 'newsletter';
    case Deleted = 'deleted';
    case Completed = 'completed';
    case Cancelled = 'cancelled';
}